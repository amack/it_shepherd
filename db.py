from os.path import exists
from os import environ
from sys import stderr


#used for dev only
'''
from sqlite3 import connect
from sqlite3 import Error as pgErr
from sqlite3 import Warning as pgWarn
'''
from flask import current_app as app
from flask import g
from psycopg2 import connect
from psycopg2 import Error as pgErr
from psycopg2 import Warning as pgWarn

def db_path():
    if 'db' not in g:
        #dbpath should be set to whatever the environmental variable
        #name that designates the database url is. this is just so
        #that different development stages can use different env
        #variable names
        g.db = environ[environ['DBPATH']]
    return g.db

def sql_connection(db):
    '''
    Create the global postgres db connection.
    '''
    if 'conn' not in g:
        try:
            g.conn = connect(db, sslmode='require')
        except pgErr as err:
            app.logger.error("Error: Could not connect to database. " +
                    str(err) + "\n", exc_info=True)
        except pgWarn as warn:
            app.logger.warning(str(warn))
        app.logger.info("Connected to database.")

    return g.conn

def sql_select(query, columns):
    '''
    Execute select query with specific data parameters and return the rows
    returned by the query.
    '''
    app.logger.debug("Creating postgres cursor.")
    conn = sql_connection(db_path())
    crs = conn.cursor()
    try:
        app.logger.debug("Executing select query.")
        crs.execute(query, columns)
    except pgErr as err:
        conn.rollback()
        app.logger.error("Query failed.\nError: {e}\nQuery: {q}".format(
                    e=str(err), q=query), 
                exc_info=True)
    except pgWarn as warn:
        app.logger.warning("Warning: " + str(warn), exc_info=True)
    

    if crs.rowcount > 0:
        fetch = crs.fetchall()
        app.logger.debug("Closing cursor.")
        crs.close()
        return fetch
    else:
        app.logger.debug("Closing cursor.")
        crs.close()
        return None

def simple_select(query):
    '''
    Execute select query with no specified data parameters.
    '''
    app.logger.debug("Creating postgres cursor.")
    conn = sql_connection(db_path())
    crs = conn.cursor()
    try:
        app.logger.debug("Executing select query.")
        crs.execute(query)
    except pgErr as err:
        conn.rollback()
        app.logger.error("Query failed.\nError: {e}\nQuery: {q}".format(
                    e=str(err), q=query),
                exc_info=True)
    except pgWarn as warn:
        app.logger.warning("Warning: " + str(warn), exc_info=True)
    
    if crs.rowcount > 0:
        fetch = crs.fetchall()
        app.logger.debug("Closing cursor.")
        crs.close()
        return fetch
    else:
        app.logger.debug("Closing cursor.")
        crs.close()
        return None

SUCCESS = True
FAIL = False

def sql_insert(query, columns):
    '''
    Execute query to insert data into a database. Returns True on success, and
    False otherwise.
    '''
    app.logger.debug("Opening postgres cursor.")
    conn = sql_connection(db_path())
    crs = conn.cursor()
    try:
        app.logger.debug("Executing insert or update query.")
        crs.execute(query, columns)
        conn.commit()
    except pgErr as err:
        conn.rollback()
        app.logger.error("Query failed.\nError: {e}\nQuery: {q}".format(
                    e=str(err), q=query),
                exc_info=True)
        return FAIL
    except pgWarn as warn:
        app.logger.warning("Warning: " + str(warn), exc_info=True)
    
    app.logger.debug("Closing cursor.")
    crs.close()

    return SUCCESS

def sql_connection_close(e=None):
    '''
    Close db connection. Run on app teardown.
    '''
    if 'conn' in g:
        conn = g.pop('conn', None)
        
        if conn is not None:
            app.logger.info("Closing database.")
            conn.close()

def init_db(flaskapp):
    '''
    App configuration and initialization
    '''
    flaskapp.teardown_appcontext(sql_connection_close)
