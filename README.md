it\_shepherd
===========

An IT support helper for Slack.

By Andrew Mack

TODO:
-----
- Order tickets by date when showing schedule.
- Only show tickets on or after the current date.
- Implement admin-only commands for closing tickets, and another command for seeing closed and open tickets.
- Implement admin-only command for modifying ticket issue type.
- Add rescheduling functionality
- More testing
- More thorough security checks
- Improve error handling (especially when slack returns errors).

Possible Features:
------------------
- Implementation of a documentation search function
- Implementation of a ticket modification function.