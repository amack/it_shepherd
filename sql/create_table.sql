CREATE TABLE IF NOT EXISTS auth(
    auth_level integer PRIMARY KEY,
    auth_desc text NOT NULL
);

CREATE TABLE IF NOT EXISTS users(
    user_id text PRIMARY KEY,
    user_auth_level integer DEFAULT 3,
    FOREIGN KEY(user_auth_level) REFERENCES auth
);

CREATE TABLE IF NOT EXISTS issues(
    issue_type integer PRIMARY KEY,
    issue_desc text UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS resolution(
    resolution_type integer PRIMARY KEY,
    resolution_desc text UNIQUE NOT NULL
);
CREATE TABLE IF NOT EXISTS form(
    form_reason text,
    form_user_id text,
    form_date date,
    form_time timetz,
    form_tech text,
    form_issue_type integer,
    form_submit boolean DEFAULT FALSE,
    PRIMARY KEY (form_user_id),
    FOREIGN KEY (form_user_id) REFERENCES users,
    FOREIGN KEY (form_tech) REFERENCES users,
    FOREIGN KEY (form_issue_type) REFERENCES issues
);

CREATE TABLE IF NOT EXISTS ticket(
    ticket_reason text NOT NULL,
    ticket_user_id text NOT NULL,
    ticket_issue_type integer DEFAULT 0,
    ticket_meet_date date,
    ticket_meet_time timetz,
    ticket_tech text,
    ticket_open timestamptz DEFAULT CURRENT_TIMESTAMP,
    ticket_close timestamptz,
    ticket_resolution_type integer DEFAULT 0,
    ticket_resolution_reason text,
    ticket_reminder text,
    PRIMARY KEY(ticket_meet_date, ticket_meet_time, ticket_tech),
    FOREIGN KEY(ticket_tech) REFERENCES users,
    FOREIGN KEY(ticket_user_id) REFERENCES users,
    FOREIGN KEY(ticket_issue_type) REFERENCES issues,
    FOREIGN KEY(ticket_resolution_type) REFERENCES resolution
);

CREATE OR REPLACE FUNCTION form_submit_check() RETURNS trigger AS
$$
BEGIN
    IF NEW.form_submit THEN
        INSERT INTO ticket (ticket_reason, ticket_user_id, ticket_issue_type,
            ticket_meet_date, ticket_meet_time, ticket_tech) VALUES
            (NEW.form_reason, NEW.form_user_id, NEW.form_issue_type,
             NEW.form_date, NEW.form_time, NEW.form_tech);
        DELETE FROM form WHERE form_user_id = NEW.form_user_id;
    END IF;
    RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER form_submit_trigger
    AFTER UPDATE ON form
    FOR EACH ROW EXECUTE FUNCTION form_submit_check();
