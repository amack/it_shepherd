class BaseITSError(Exception):

    def __init__(self, message: str):
        self.message = message

class AuthNotOKError(BaseITSError):
    '''
    For when slack api's auth.test command doesn't return ok.
    '''

class NoConnectionError(BaseITSError):
    '''
    For when an action requiring an active connection is made when no
    connection is present.
    '''

class CreateDBError(BaseITSError):
    '''
    For when creation of the database in intitalize.py fails.
    '''

class NoUserError(BaseITSError):
    '''
    Raised when no user can be identified as having sent a message.
    '''

class BadActionError(BaseITSError):
    '''
    Raised when an action is received from a block message that is invalid.
    '''

class CreateTicketError(BaseITSError):
    '''
    Raised when there is an error creating a support ticket.
    '''

class IncompleteTicketError(CreateTicketError):
    '''
    Raised when a user tries to create a support ticket while they already have
    one in the queue.
    '''
