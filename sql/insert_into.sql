INSERT INTO auth (auth_level, auth_desc) VALUES
    (-1, 'Banned'),
    (0, 'Owner'),
    (1, 'Admin'),
    (2, 'Technician'),
    (3, 'User');
INSERT INTO issues (issue_type, issue_desc) VALUES
    (0, 'Other'),
    (1, 'Hardware Issue'),
    (2, 'Software Issue'),
    (3, 'Purchase Request'),
    (4, 'System Administration'),
    (5, 'Networking'),
    (6, 'Unsure');

INSERT INTO resolution (resolution_type, resolution_desc) VALUES
    (0, 'open'),
    (1, 'resolved'),
    (2, 'cant Fix'),
    (3, 'wont Fix'),
    (4, 'non-Issue');

