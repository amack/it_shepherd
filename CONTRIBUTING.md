# Contributing

## Required Tools:
- Python 3.6.8
- Python Libraries:
    - Flask 1.0.2
    - slackclient 1.3.1
    - slackeventsapi 2.1.0
    - psycopg2 2.8.2
    - pytz 2018.4
    - These can all be downloaded via pip
- Postgresql 11
- yapf
    - This can also be downloaded via pip, however its not used as a library but for formatting the code.
- Heroku CLI (If admining heroku)
- Slack (technically not required, but I don't know how you'd run this without it.)

## Code style:
- Use yapf to format code. In the project root directory, run this to format your code: 
```bash
yapf -d *.py
```
- 4 spaces for tabs
- Lines should extend no longer than 80 characters.
- If you have a string literal longer than 80 characters, define it outside of the function it is in so as not to hurt readability.
- Prefer creating functions over classes and methods
- As long as the above is followed, you're free to format code however you feel is most readable. 
    - Should the need arrise to break these guidelines, explain why with comments.