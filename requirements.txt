# required packages
flask >= 1.0.2
slackclient == 1.3.1
slackeventsapi >= 2.1.0
psycopg2 >= 2.8.2
pytz >= 2018.4
