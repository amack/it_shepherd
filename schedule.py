from re import search
from datetime import date
from os.path import join 

from flask import current_app as app

from .blocks import load_block, block_post
from .db import sql_select
from .bot import rtmPost, client
from .shepherderror import BaseITSError

SCHED_USER = """SELECT ticket_meet_date, ticket_meet_time, ticket_tech, 
                ticket_reason FROM ticket WHERE (ticket_user_id = %s OR 
                ticket_tech = %s) AND (ticket_meet_date >= CURRENT_DATE);"""
SCHED_DATE = """SELECT ticket_user_id, ticket_meet_time, ticket_tech, 
                ticket_reason FROM ticket WHERE (ticket_meet_date = %s) AND
                (ticket_meet_date >= CURRENT_DATE);"""

def schedule_block(sched, arg):

    if sched == 'date':
        blockpath = join(app.instance_path, "json/schedule_date.json")
        block = load_block(blockpath)
        block[0]['text']['text'] = block[0]['text']['text'].format(d=arg)
        rows = sql_select(SCHED_DATE, (arg,))

        if rows is not None:
            app.logger.debug("Rows found for SCHED_DATE query.")
            fields = None
            for row in rows:
                username = row[0]
                userinfo = client().api_call(
                        "users.info",
                        user=username
                    )
                if 'ok' in userinfo.keys():
                    if userinfo['ok']:
                        username = userinfo['user']['name']
                tech = row[2]
                techinfo = client().api_call(
                        "users.info",
                        user=tech
                    )
                if 'ok' in techinfo.keys():
                    if techinfo['ok']:
                        tech = techinfo['user']['name']
                timetz = row[1].strftime("%I:%M %p")
                fields = [ {"type":"mrkdwn", "text":username},
                    {"type":"mrkdwn", "text":timetz},
                    {"type":"mrkdwn", "text":tech},
                    {"type":"mrkdwn", "text":row[3]}
                ]
            
                section = {"type":"section", "fields":fields}
                block.append(section)
            return block
        
        else:
            app.logger.debug("Rows not found for SCHED_DATE query.")
            section = {"type":"section", "text":{
                "type":"mrkdwn", "text":"No tickets found."}}
            block.append(section)
            return block
                 
    elif sched == 'user':
        blockpath = join(app.instance_path, "json/schedule_user.json")
        block = load_block(blockpath)
        userinfo = client().api_call(
                "user.info",
                user=arg
            )
        username = arg
        if 'ok' in userinfo.keys():
            if userinfo['ok']:
                username = userinfo['user']['name']
        block[0]['text']['text'] = block[0]['text']['text'].format(u=username)
        rows = sql_select(SCHED_USER, (arg, arg))

        if rows is not None:
            app.logger.debug("Rows found for SCHED_USER query.")
            for row in rows:
                tech = row[2]
                techinfo = client().api_call(
                            "users.info",
                            user=tech
                        )
                if 'ok' in techinfo.keys():
                    if techinfo['ok']:
                        tech = techinfo['user']['name']
                timetz = row[1].strftime("%I:%M %p")
                fields = [ {"type":"mrkdwn", "text":row[0].isoformat()},
                    {"type":"mrkdwn", "text":timetz},
                    {"type":"mrkdwn", "text":tech},
                    {"type":"mrkdwn", "text":row[3]}
                ]
            
                section = {"type":"section", "fields":fields}
                block.append(section)
            return block

        else:
            app.logger.debug("Rows not found for SCHED_USER query.")
            section = {"type":"section", "text":{
                "type":"mrkdwn", "text":"No tickets found."}}
            block.append(section)
            return block

    else:
        raise KeyError("Key {s} not in 'SCHEDULES'.".format(s=sched))

RE_ME = r"(^|[\s]*)me[\s]*(.*)"
RE_USER = r"(^|[\s]*)<@(|[WU].+?)>[\s]*(.*)"
RE_DATE = r"(^|[\s]*)[0-9]{4}-[0-9]{2}-[0-9]{2}[\s]*(.*)"

def schedule_request(channel, user, args):
    
    if args is None:
        day = date.today()
        args = day.isoformat()
    else:
        args = args.strip().lower()
    
    match = search(RE_ME, args)
    if match:
        app.logger.debug("RE_ME regex matched.")
        block = schedule_block('user', user)
        match = None
        return block_post(channel, block)
    
    match = search(RE_USER, args)
    if match:
        app.logger.debug("RE_USER regex matched.")
        block = schedule_block('user', args)
        match = None
        return block_post(channel, block)
    
    match = search(RE_DATE, args)
    if match:
        app.logger.debug("RE_DATE regex matched.")
        block = schedule_block('date', args)
        match = None
        return block_post(channel, block)

    app.logger.debug("No regex matched.")
