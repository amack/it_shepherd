from os.path import exists, expanduser, join, isfile, relpath
from os import mkdir, listdir, environ
from sys import stderr, exit
#only used for dev
'''
from sqlite3 import connect, Error
from sqlite3 import Warning as sqlWarn
'''
from re import fullmatch
from functools import wraps
from shutil import copy2

from flask import current_app, g
from flask.cli import with_appcontext
from click import command, echo
from psycopg2 import connect
from psycopg2.extras import execute_batch
from psycopg2 import Error as pgErr
from psycopg2 import Warning as pgWarn

USERID_REGEX = "^([WU].+?)"

def init_app(app):
    '''
    Configuring and initializing the app.
    '''
    app.cli.add_command(initdb_cmd)
    app.cli.add_command(testdb_cmd)
    app.cli.add_command(dropdb_cmd)

from .shepherderror import CreateDBError

def initdb():
    '''
    Command line function for creating the database. Should be run before the
    app is run.
    '''
    res = 0
    if 'db' not in g:
        g.db = environ[environ['DBPATH']]
    
    if 'db' in g:
        conn = connect(g.db, sslmode='require')
        crs = conn.cursor()

        create = None
        with current_app.open_resource('sql/create_table.sql') as f:
            create = f.read().decode('utf8')

        try:
            crs.execute(create)
            conn.commit()
        except pgErr as err:
            conn.rollback()
            stderr.write(str(err) + '\n')
            res = 1
        except pgWarn as warn:
            stderr.write(str(warn) + '\n')

        insert = None
        with current_app.open_resource('sql/insert_into.sql') as f:
            insert = f.read().decode('utf8')
            f.close()
        
        try:
            crs.execute(insert)
            conn.commit()
        except pgErr as err:
            conn.rollback()
            stderr.write(str(err) + '\n')
            res = 1
        except pgWarn as warn:
            stderr.write(str(warn) + '\n')

        ownid = None
        while ownid is None: 
            ownid = input("Enter Owner's user ID: ")
            if ownid is None:
                print("""
                        Enter a correctly formated user ID.
                        User ID Regex: {reg}
                    """.format(reg=USERID_REGEX))
        
        try:
            crs.execute("INSERT INTO users VALUES (%s, 0)", (ownid,))
            conn.commit()
        except pgErr as err:
            conn.rollback()
            stderr.write(str(err) + '\n')
            res = 1
        except pgWarn as warn:
            stderr.write(str(warn) + '\n')

        conn.close()

    else:
        raise CreateDBError("Database could not be created.\n")
    return res

'''
Wrapping the initdb() function for use on the command line.
'''
@command('initdb')
@with_appcontext
def initdb_cmd():
    try:
        initdb()
    except FileNotFoundError as err:
        stderr.write("Creation schema could not be found.\n")
        return 1
    except CreateDBError as err:
        stderr.write(err.message + "\n")
        return 1
    echo("Database creation succesful.")
    return 0

from .db import sql_select, sql_insert
from .shepherderror import CreateTicketError

def testdb():
    
    user = "test_user"
    echo("Inserting test_user into db.")
    sql_insert("INSERT INTO  users VALUES (%s, 2);", (user,))

    insert = '''
            INSERT INTO form (form_reason, form_user_id, form_date, 
                form_time, form_issue_type)
            VALUES (%s, %s, %s, %s, %s);
            '''
    reason = "test"
    date = "2019-04-23"
    time = "09:00 AM"
    issue = 1
    data = (reason, user, date, time, issue)
    
    echo("Inserting test form into db.")
    res = sql_insert(insert, data)

    if not res:
        raise CreateTicketError("Could not create new form.")

    update = 'UPDATE form SET form_submit = TRUE WHERE (form_user_id = %s);'
    
    echo("Updating form_submit value for test form to submit the ticket.")
    res = sql_insert(update, (user,))

    if not res:
        raise CreateTicketError("Could not submit ticket.")
    
    select = 'SELECT * FROM ticket WHERE (ticket_user_id = %s);'
    
    echo("Selecting the newly submitted ticket from the db.")
    rows = sql_select(select, (user,))
    
    if rows is not None:
        for row in rows:
            for col in row:
                print(col)
    
    echo("""Selecting test form from db to ensure it was removed by the trigger
            function. If succesful, the select will produce no rows.""")
    select = 'SELECT * FROM form WHERE (form_user_id = %s);'
    rows = sql_select(select, (user,))

    if rows is not None:
        echo("Test failed.")
    else:
        echo("Test successful.")

    return 0

@command('testdb')
@with_appcontext
def testdb_cmd():
    try:
        testdb()
    except CreateTicketError as err:
        stderr.write(str(err) + "\n")
        return 1
    echo("Database test succesful.")
    return 0

from sys import stdin
from .db import sql_connection, db_path

def dropdb():

    res = 0

    answer = input(
            """WARNING: THIS WILL DROP ALL ROWS IN THE DATABASE. 
            ARE YOU SURE YOU WANT TO CONTINUE? (y/n): """)
    if answer.lower() not in ['yes', 'y']:
        return -1
    
    with current_app.open_resource('sql/drop_table.sql') as f:
        drop = f.read().decode('utf8')
    conn = sql_connection(db_path())
    crs = conn.cursor()
    try:
        crs.execute(drop)
        conn.commit()
    except pgErr as err:
        conn.rollback()
        stderr.write(str(err))
        res=1
    except pgWarn as warn:
        stderr.write(str(warn))

    conn.close()

    return res

@command('dropdb')
@with_appcontext
def dropdb_cmd():
    res = dropdb()
    if res == -1:
        print("Canceled.")
        return 0
    else:
        return res
