from os import makedirs, environ, urandom
from os.path import join, exists
from sys import stdout, stderr
from logging import basicConfig
from datetime import timedelta

from flask import Flask, g, request, session
#from flask_login import LoginManager, UserMixin, login_user
from slackeventsapi import SlackEventAdapter

from .bot import chatlistener
from .support import interactivity_webhook

def create_app(test_config=None):
    '''
    Set the app up and run it.
    '''
    
    basicConfig(format="[%(asctime)s][%(levelname)s]: %(message)s",
            level=environ['LOG_LVL'], stream=stdout)

    app = Flask(__name__, instance_path='/app')

    app.secret_key = environ['APP_SECRET']

    #this just confirms the instance path exists
    try:
        makedirs(app.instance_path)
    except OSError:
        pass
 
    from .initialize import init_app
    init_app(app)

    from .db import init_db
    init_db(app)

    @app.route('/', methods=['GET', 'POST'])
    def homepage():
        return "It works!"

    @app.route('/slack', methods=['GET', 'POST'])
    def interactivity():
        return interactivity_webhook(request)

    slack_events_adapter = SlackEventAdapter(environ['SIGNING_SECRET'], 
            '/slack/events', app)

    @slack_events_adapter.on("app_mention")
    def chat_listener(event):
        return chatlistener(event['event'])

    return app
