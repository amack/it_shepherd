from os.path import join
from os import environ
from datetime import timedelta, date

from flask import current_app as app
from flask import g

from .blocks import load_block, block_post
from .db import sql_select, sql_insert, simple_select
from .shepherderror import IncompleteTicketError
from .bot import client

def getUserInfo(user):
    info = client().api_call(
            "users.info",
            user=user
        )
    if 'error' in info.keys():
        app.logger.warning("Slack client returned error " + info['error'])
        return None
    return info

FIND_TECH = """SELECT user_id FROM users WHERE user_auth_level <= 1;"""

def support_request(channel, userid, reason):
    '''
    Send out the actual ticket form.
    '''
    today = date.today().isoformat()
    rows = sql_select(
            "SELECT form_user_id FROM form WHERE (form_user_id = %s);",
            (userid,))

    if rows is not None:
        raise IncompleteTicketError(
            "User attempted to create a ticket without finishing another.")
    
    app.logger.debug("Adding form data for user {u} to database.".format(
                u=userid))
    insert = """INSERT INTO form 
                (form_user_id, form_date, form_reason, form_tech) VALUES 
                (%s, %s, %s, %s);"""
    admin = simple_select("SELECT user_id FROM users WHERE user_auth_level=0;")
    tech = None
    if admin is not None:
        tech = admin[0][0]
    sql_insert(insert, (userid, today, reason, tech))
    
    formpath = join(app.instance_path, 'json/support_request.json')
    block = load_block(formpath)
    info = getUserInfo(tech)
    option = {
        "text":{
            "type":"plain_text",
            "text":info['user']['name'],
            "emoji":True
        },
        "value":tech
    }
    block[3]["elements"][0]['options'].append(option)
    techs = simple_select(FIND_TECH)
    if techs is not None:
        for row in techs:
            info = getUserInfo(row[0])
            username = info['user']['name']
            id = info['user']['id']
            option = {
                        "text":{
                            "type":"plain_text",
                            "text":username,
                            "emoji":True
                        },
                        "value":id
                    }   
            block[3]["elements"][0]["options"].append(option)
    #Sets default calendar date to the current day.
    block[5]["elements"][0]["initial_date"] = today
    block_post(channel, block, True, userid)
    app.logger.debug("Support request form posted.")
    return ('', 200, None)

from json import loads
from .bot import token

NO_TX_WARN = "User {u} attempted to update a non-exsistant ticket."

def interactivity_webhook(request):
    '''
    Listen for payloads when a user interacts with the form sent in 
    support_request().
    '''
    app.logger.debug("Interactivity webhook received.")
    payload = loads(request.form['payload'])
    #print(payload)
    paykeys = payload.keys()

    if 'type' in paykeys:

        if payload['type'] == "url_verification" and 'challenge' in paykeys:
            return (payload['challenge'], 200, None)

        elif payload['type'] == "block_actions":
            user = payload["user"]["id"]
            rows = sql_select(
                    "SELECT form_user_id FROM form WHERE (form_user_id = %s);",
                    (user,))

            if rows is None:
                app.logger.warning(NO_TX_WARN.format(u=payload['user']['id']))
                return ('', 403, None)

            action = payload['actions'][0]
            #print(action)

            return handle_action(user, payload['response_url'], action, 
                    payload['channel'])
    
    else:
        return ('', 403, None)

from json import dumps
from .shepherderror import BadActionError
from .bot import schedulePost, rtmPost

TIME_SEL = '''SELECT form_time, form_tech FROM form WHERE form_user_id = %s 
                AND form_time IS NOT NULL AND form_tech IS NOT NULL;'''
DATE_SEL = '''SELECT form_date, form_tech FROM form WHERE form_user_id = %s 
                AND form_date IS NOT NULL AND form_tech IS NOT NULL;'''
TECH_SEL = '''SELECT form_date, form_time FROM form WHERE (form_user_id = %s) 
                AND form_date IS NOT NULL AND form_time IS NOT NULL;'''
FORM_ISU = 'UPDATE form SET form_issue_type = %s WHERE (form_user_id = %s);'
FORM_DAT = 'UPDATE form SET form_date = %s WHERE (form_user_id = %s);'
FORM_TIM = 'UPDATE form SET form_time = %s WHERE (form_user_id = %s);'
FORM_TEC = 'UPDATE form SET form_tech = %s WHERE (form_user_id = %s);'
FORM_SUB = 'UPDATE form SET form_submit = TRUE WHERE (form_user_id = %s);'
FORM_DBG = 'User {u} attempted to update form with NON-EXSISTANT FIELD {f}.'
FORM_DEL = 'DELETE FROM form WHERE (form_user_id = %s);'
SUB_TIME = '''SELECT form_date, form_time, form_tech, form_reason FROM form 
            WHERE (form_user_id = %s);'''
TCKT_PID = '''UPDATE ticket SET ticket_reminder = %s WHERE (ticket_meet_date = %s)
                AND (ticket_meet_time = %s) AND (ticket_tech = %s);'''
REMINDER = """<@{u}>, this is a reminder for your meeting scheduled with <@{a}>. 
Ticket reason: ```{r}```"""
TECH_ALERT = "New support request by user <@{u}> on {d} at {t}. Reason: '{r}'"

def handle_action(user, url, action, channeldata):
    '''
    Handle the interaction of the user with the support ticket form.
    '''
    channel = channeldata['id']
    app.logger.debug("Action received from user {u}.".format(u=user))
    status = 200
    badtimestr = "Chosen meeting time unavailable. Please choose another."
    id = action['action_id']
    submitfail = "Error submitting ticket. Please alert an admin."
    
    '''
    Note that we use the sql_insert function to process UPDATE queries. This is
    because despite its name, sql_insert can handle any query that does not
    return rows.
    '''
    if id == 'issue_type':
        value = action['selected_option']['value']
        res = sql_insert(FORM_ISU, (value, user))
        if not res:
            status = 500
    
    elif id == 'date':
        value = action['selected_date']
        field = "form_time"
        rows = sql_select(TIME_SEL, (user,))
        available = True

        if rows is not None:
            time = rows[0][0]
            tech = rows[0][1]

            available = timeAvailable(value, time, tech)
        
        if available:
            sql_insert(FORM_DAT, (value, user))
        else:
            rtmPost(channel, badtimestr, True, user)
    
    elif id == 'time': 
        value = action['selected_option']['value']
        field = "form_date"
        rows = sql_select(DATE_SEL, (user,))
        available = True
        if rows is not None:
            date = rows[0][0]
            tech = rows[0][1]
            available = timeAvailable(date, value, tech)
        
        if available:
            sql_insert(FORM_TIM, (value, user))
        else:
            rtmPost(channel, badtimestr, True, user)

    elif id == 'technician':
        value = action['selected_option']['value']
        field = 'form_tech'
        rows = sql_select(TECH_SEL, (user,))
        available = True
        if rows is not None:
            date = rows[0][0]
            time = rows[0][1]
            available = timeAvailable(date, time, tech)

        if available:
            sql_insert(FORM_TEC, (value, user))
        else:
            rtmPost(channel, badtimestr, True, user)

    elif id == 'submit':
        select = sql_select(SUB_TIME, (user,))
        success = sql_insert(FORM_SUB, (user,))
        if not success:
            app.logger.warning("Ticket submission failed for user {u}.".format(
                        u=user))
            rtmPost(channel, 
                    "Form could not be submitted. Please contact an admin.",
                    private=True, user=user)
        else:
            app.logger.debug(
                    "Ticket submission for user {u} succesful.".format(u=user))
            rtmPost(channel, "Form submitted successfully.", private=True, 
                user=user)
            if select is not None:
                date = select[0][0]
                time = select[0][1]
                tech = select[0][2]
                reason = select[0][3]
                postid = schedulePost(channel, date, time, REMINDER.format(
                           u=user, a=tech, r=reason))
                sql_insert(TCKT_PID, (postid, date, time, tech))
                iminfo = client().api_call(
                        "im.open",
                        user=tech
                    )
                imid = None
                if 'channel' in iminfo:
                    imid = iminfo['channel']['id']
                if imid is not None:
                    client().api_call(
                            "chat.postMessage",
                            channel=imid,
                            text=TECH_ALERT.format(u=user,
                            d=date, t=time, r=reason),
                            as_user=True
                        )
                else:
                    app.logger.warning(
                        "Could not open an im with technician '{t}'.".format(
                            t=tech))
            else:
                app.logger.warning(
                        "Reminder could not be scheduled for user {u}.".format(
                            u=user))
                rtmPost(channel,
                        "Could not create reminder. Please alert an admin.",
                        private=True, user=user)

    elif id == 'cancel':
        sql_insert(FORM_DEL, (user,))
        app.logger.debug("Form for user {u} deleted.".format(u=user))
        rtmPost(channel, "Support request canceled.", True, user)

    else:
        app.logger.debug(FORM_DBG.format(u=user, f=id))
        status = 404

    return ('', status, None)

from datetime import datetime, time
from pytz import timezone

def timeAvailable(day, tim, tech):
    '''
    Check to see if a chosen meeting time is avaliable.
    '''
    if tim is None or day is None or tech is None:
        return True
    
    if isinstance(tim, str):
        timsplit= tim.split(':')
        tim = time(int(timsplit[0]), int(timsplit[1]))
    testtime = datetime.timestamp(datetime.combine(day, tim, 
                timezone(environ['PGTZ'])))
    now = datetime.timestamp(datetime.now(timezone(environ['PGTZ'])))

    if testtime < now:
        return False


    data = (day, tim, tech, tech)
    qstr = """
            SELECT * FROM ticket WHERE 
            ticket_meet_date = %s AND ticket_meet_time = %s
            AND (ticket_tech = %s OR ticket_user_id = %s);
            """
    fetch = sql_select(qstr, data)
    if fetch is not None:
        if len(fetch) > 0:
            return False
    return True

TX_SEL = """SELECT ticket_meet_date, ticket_meet_time, ticket_tech FROM ticket
            WHERE (ticket_meet_date = %s) AND (ticket_meet_time = %s) 
            AND (ticket_tech = %s);"""

def find_ticket(tech, day, tim, ampm=None):
    splittime = tim.split(':')
    hour = int(splittime[0])
    minute = int(splittime[1])
    if ampm is not None:
        if 'pm' in ampm.lower() and hour <= 12:
            hour += 12
    tim = time(hour, minute)
    rows = sql_select(TX_SEL, (day, tim, tech))
    return rows

SQL_CANCEL = """DELETE FROM ticket WHERE (ticket_meet_date = %s) 
                AND (ticket_meet_time = %s) AND (ticket_tech = %s);"""
DEL_ERR = "Something went wrong while canceling your request."

def support_cancel(channel, user, tech, day, tim, ampm):
    
    rows = find_ticket(tech, day, tim, ampm)

    if rows is not None:
        for row in rows:
            reminder = sql_select("""SELECT ticket_reminder FROM ticket WHERE
                        (ticket_meet_date = %s) AND (ticket_meet_time = %s)
                        AND (ticket_tech = %s);""",
                        (row[0], row[1], row[2]))
            res = client().api_call(
                "chat.deleteScheduledMessage",
                channel=channel,
                scheduled_message_id=reminder[0][0],
            )
            app.logger.debug("Scheduled message {m} deleted.".format(
                        m=reminder[0][0]))

            sqlres = sql_insert(SQL_CANCEL, (row[0], row[1], row[2]))
            
            #This just checks to make sure there's an 'ok' field period,
            #Because even if Slack returns an error and res['ok'] is false,
            #we have still done our job properly.
            if 'ok' in res.keys() and sqlres:
                rtmPost(channel, "Request succesfully canceled.")
                return 0
            #This returns false so that cmdhandle() knows to return a 404,
            #since we only want responses containing "ok" fields.
            elif 'ok' not in res.keys():
                app.logger.warning("Slack did not return 'ok'.")
                rtmPost(channel, DEL_ERR)
                return 1

            elif not sqlres:
                app.logger.warning("Could not delete ticket from databse.")
                rtmPost(channel, DEL_ERR)
                return -1
    else:
        rtmPost(channel, "No ticket found at specified time and date.", True, 
                user)
        return True

RES_TYPE = "SELECT resolution_type FROM resolution WHERE resolution_desc = %s;"

TX_CLOSE = """UPDATE ticket SET ticket_close = CURRENT_TIMESTAMP, 
            ticket_resolution_type = %s, ticket_resolution_reason = %s 
            WHERE ticket_meet_date = %s AND ticket_meet_time = %s 
            AND ticket_tech = %s;"""

CLOSE_ALERT = """<@{u}>, your ticket with reason '{r}' has been marked as resolved by technician <@{t}> with resolution type '{e}'.
Reason for closing: {c}"""

def support_close(channel, tech, day, tim, resolution, reason):
    
    rows = sql_select(RES_TYPE, (resolution,))
    resnum = 1
    if rows is not None:
        resnum = rows[0][0]
        app.logger.debug("Resolution type found.")

    rows = find_ticket(tech, day, tim)
    if rows is not None:
        app.logger.debug("Ticket to close found.")
        for row in rows:
            day = row[0]
            tim = row[1]
            tech = row[2]
            data = (resnum, reason, day, tim, tech)
            res = sql_insert(TX_CLOSE, data)
            if res:
                user = None
                tx_reason = None
                rows = sql_select("""SELECT ticket_user_id, ticket_reason 
                        FROM ticket WHERE ticket_meet_date = %s 
                        AND ticket_meet_time = %s AND ticket_tech = %s;""",
                        (day, tim, tech))
                if rows is not None:
                    user = rows[0][0]
                    tx_reason = rows[0][1]
                rtmPost(channel, "Ticket succesfully closed.", private=True,
                        user=tech)
                rtmPost(channel, CLOSE_ALERT.format(u=user, r=tx_reason, t=tech,
                            e=resolution, c = reason))
                app.logger.debug("Ticket closed succesfully.")
                return 0
            else:
                app.logger.debug("Ticket found but not closed.")
                rtmPost(channel, 
                        "Ticket was found, but there was a problem closing it.",
                        private=True, user=tech)
                return 1

    else:
        app.logger.debug("Could not find ticket.")
        rtmPost(channel, "Could not find specified ticket.", private=True, user=tech)
        return -1
