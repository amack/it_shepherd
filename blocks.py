from json import loads
from os.path import join

from flask import current_app as app

def load_block(blockpath):
    '''
    Load a json object containing the block message for slack.
    '''
    block = None
    app.logger.debug("Trying to open json block file.")
    try:
        with open(blockpath) as blockfile:
            blockstr = str(blockfile.read())
            #app.logger.debug("Block: {b}".format(b=blockstr))
            block = loads(blockstr)
            blockfile.close()
    except OSError as err:
        strerr = str(err)
        app.logger.error('Could not load {f}: {s}'.format(f=err.filename, 
                    s=strerr), exc_info=True)
    
    app.logger.debug("Block loaded.")
    return block

from .bot import client

def block_post(channel, block, private=False, user=None):
    '''
    Post a block message to slack.
    '''
    res = None
    if private and user is not None:
        res = client().api_call(
                "chat.postEphemeral",
                channel=channel,
                user=user,
                blocks=block
            )
        app.logger.debug("Posting ephemeral block.")
    else:
        res = client().api_call(
                "chat.postMessage",
                channel=channel,
                text="Could not display content.",
                blocks=block
            )
        app.logger.debug("Posting block.")

    if res is None:
       app.logger.warning("Slack did not respond to api call.")
       return None

    if res is not None and 'ok' in res.keys():
        if not res['ok']:
            app.logger.warning(
                    "Slack client api call returned error {e}.".format(
                        e=res['error']))
        else:
            if 'warning' in res.keys():
                app.logger.warning(
                        "Slack client api call returned warning {w}.".format(
                            w=res['warning']))
            else:
                app.logger.debug(
                        "Block successfully sent to channel {c}.".format(
                            c=channel))
        return res['ok']
    else:
        app.logger.warning(
                "Slack did not send a response containing an 'ok' field.")
        return None


from flask import Blueprint
from .bot import rtmPost

def post_block(channel, path):
    '''
    Post a block message.
    '''
    blockfile = join(app.instance_path, path)

    loadedblock = load_block(blockfile)

    if loadedblock is None:
        rtmPost(channel, "Could not load message.")
        app.logger.warning("Could not load block.")
    else:
        app.logger.debug("Block loaded.")
        block_post(channel, loadedblock)

    return ('', 200, None)
