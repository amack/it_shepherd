COMMAND = {
    'default':0,
    'off':1,
    'help':2,
    'support':3,
    'schedule':4,
    'doc':5,
    'cancel':6,
    'close': 7,
    'about':8
}

from sys import stdout

from flask import current_app as app

from .bot import rtmPost 
from .db import sql_select, sql_insert

CMD_INVALID="""
            Invalid command. Use the command 'help' to see a list of commands.
            """
def cmdInvalid(channel):
    rtmPost(channel, CMD_INVALID)
    return ('', 200, None)

NOT_IMPLEMENTED="Command not yet implemented."

def notImplemented(channel):
    rtmPost(channel, NOT_IMPLEMENTED)
    return ('', 200, None)

from re import search

from .blocks import post_block
from .support import support_request, support_cancel, support_close
from .schedule import schedule_request, RE_DATE
from .shepherderror import IncompleteTicketError

RE_AMPM = r"(^|[\s]*)([aA]|[pP])[mM][\s]*(.*)"
RE_TIME = r"(^|[\s]*)(|[0-2])[0-9]:[0-5][0-9][\s]*(.*)"
RE_MENTION = r"^<@(|[WU].+?)>(.*)"
COULD_NOT_FIND = "Could not find a date, time, and/or tech mention in given message."
BANNED = "Banned user {u} attempted to use command '{c}' with arguments '{a}'."

def cmdHandle(commandstr, infodict):
    '''
    Handle commands from users.
    '''
    user = infodict['user']
    
    if 'channel' in infodict.keys():
        channel = infodict['channel']
    else:
        channel = user
    
    select = "SELECT user_auth_level FROM users WHERE user_id = %s;"
    app.logger.debug("Retrieving user auth level from database.")
    userdata = sql_select(select, (user,))
    uauth = 3
    if userdata is None:
        app.logger.debug("User not found, inserting into database.")
        insert = "INSERT INTO users VALUES (%s, %s);"
        sqldata = (user, uauth)
        sql_insert(insert, sqldata)
    else:
        app.logger.debug("User data found.")
        uauth = userdata[0][0]

    command = commandstr.split(None, 1)
    cmdArgs = None

    cmdStr = command[0].lower()
    if len(command) > 1:
        cmdArgs = command[1]

    if uauth < 0:
        rtmPost(channel, "Permission denied.")
        app.logger.info(BANNED.format(u=user, c=command, a=cmdArgs))
        return ('', 200, None)
    
    if cmdStr not in COMMAND.keys():
        app.logger.debug("Command not found.")
        return cmdInvalid(channel)

    cmd = COMMAND[cmdStr]

    if cmd is COMMAND['off'] and 0 <= uauth <= 1:
        stdout.write("App going down.")
        app.app_context().pop()
    
    elif cmd is COMMAND['help']:
        app.logger.debug("Handling 'help' command.")
        return post_block(channel, 'json/help.json')

    elif cmd is COMMAND['about']:
        app.logger.debug("Handling 'about' command.")
        return post_block(channel, 'json/about.json')

    elif cmd is COMMAND['support']:
        app.logger.debug("Handling 'support' command.")
        res = ('', 200, None)
        try:
            res = support_request(channel, user, cmdArgs)
        except IncompleteTicketError as err:
            rtmPost(channel, err.message)
            return ('', 403, None)
        return res

    elif cmd is COMMAND['schedule']:
        app.logger.debug("Handling 'schedule' command.")
        res = schedule_request(channel, user, cmdArgs)
        if res:
            return ('', 200, None)
        else:
            rtmPost(channel, "Schedule could not be retrieved.")
            return ('', 200, None)

    elif cmd is COMMAND['doc']:
        app.logger.debug("Handling 'doc' command.")
        return notImplemented(channel)
    
    elif cmd is COMMAND['cancel']:
        app.logger.debug("Handling 'cancel' command.")
        splitargs = cmdArgs.split()
        if len(splitargs) < 4:
            rtmPost(channel, COULD_NOT_FIND)
            return ('', 200, None)
        for i in range(len(splitargs)):
            splitargs[i] = splitargs[i].strip()
        
        day = None
        for i in range(len(splitargs)):
            match = search(RE_DATE, splitargs[i])
            if match:
                day = splitargs[i]
                app.logger.debug("Day of ticket found.")
                break

        tim = None
        for i in range(len(splitargs)):
            match = search(RE_TIME, splitargs[i])
            if match:
                tim = splitargs[i]
                app.logger.debug("Time of ticket found.")
                break
        
        ampm = None
        for i in range(len(splitargs)):
            match = search(RE_AMPM, splitargs[i])
            if match:
                ampm = splitargs[i]
                app.logger.debug("AM or PM found.")
                break

        tech = None
        for i in range(len(splitargs)):
            match = search(RE_MENTION, splitargs[i])
            if match:
                tech = splitargs[i][2:-1]
                app.logger.debug("Tech id found")
                break
        
        res = 0
        if day is not None and tim is not None and tech is not None:
            res = support_cancel(channel, user, tech, day, tim, ampm)
        else:
            rtmPost(channel, COULD_NOT_FIND)

        if res == 0:
            return ('', 200, None)
        elif res == 1:
            return ('', 404, None)
        else:
            return ('', 500, None)

    elif cmd is COMMAND['close'] and 0 <= uauth <= 2:
        app.logger.debug("Handling 'close' command.")
        splitargs = cmdArgs.split(None, 3)
        if len(splitargs) < 4:
            rtmPost(channel, "Could not close: Too few arguments provided.")
            return ('', 200, None)
        daytim = [splitargs[0], splitargs[1]]
        day = None
        tim = None
        for i in range(len(daytim)):
            match = search(RE_DATE, daytim[i])
            if match:
                day = daytim[i]
            match = search(RE_TIME, daytim[i])
            if match:
                tim = daytim[i]
        if day is None:
            rtmPost(channel, "Could not close: No date provided.")
            return ('', 200, None)
        if tim is None:
            rtmPost(channel, "Could not close: No time provided.")
            return ('', 200, None)

        resolution = splitargs[2].lower()
        rows = sql_select("SELECT resolution_desc FROM resolution WHERE resolution_desc = %s;", (resolution,))
        if rows is None:
            rtmPost(channel, "Invalid resolution type.")
            return ('', 200, None)

        res = support_close(channel, user, day, tim, resolution, splitargs[3])
        if res == 1:
            return ('', 500, None)
        else:
            return ('', 200, None)

    else:
        app.logger.debug("Handling invalid command.")
        return cmdInvalid(channel)
